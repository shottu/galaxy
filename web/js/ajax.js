$( document ).ready(function() {

	// Ajax request register
	$("#register-form").submit(function (e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/app/user.php",
			data: $(this).serialize(),
			success: function (response) {
				var data = JSON.parse(response);
				if (data.register == 'error_pass') {
					$("#error_pass").text('Error. Password must be more than 8 characters');
				}
				if (data.register == true) {
					contentUser("#register-form", "#error_pass", data.user);
				}
			}
		});
	});

	// Ajax request login
	$('#login-form').submit(function (e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/app/user.php",
			data: $(this).serialize(),
			success: function (response) {
				var data = JSON.parse(response);
				if(data.login == 'error_login') {
					$("#error_login").text("Error. Wrong login or password");
				}
				if(data.login == true) {
					contentUser("#login-form", "#error_login", data.user);
				}
			}
		});
	});

	// front content user
	function contentUser(form, errors, data) {
		$(form).trigger("reset");

		$(errors).empty();

		$(form).hide();

		$(".content-user").append('<h3>Hello, ' + data + '</h3>' + '<p class="logout">Выйти</p>');							
		$('.logout').click(function() {
			$('#login-form').show();
			$(".content-user").empty();
		});
	}
});