
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/web/js/ajax.js"></script>
    <script type="text/javascript" src="/web/js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="/web/css/style.css">
    <title>Galaxy</title>
</head>
<body>
    <center>
            <form method="POST" id="register-form">
                <h3>Welcome, register please.</h3>

                <p>Login:</p>
                <input type="text" name="reg_login" placeholder="Login">

                <p>Password:</p>
                <input type="password" name="reg_pass" placeholder="Password">
                <div id="error_pass"></div>

                <p>
                    <button>Register</button>
                </p>
                <p class="link_login">
                    Login
                </p>
            </form>

            <form method="POST" id="login-form">
                <h3>Welcome, login please.</h3>

                <p>Login:</p>
                <input type="text" name="login" placeholder="Login">

                <p>Password:</p>
                <input type="password" name="pass" placeholder="Password">
                <div id="error_login"></div>

                <p>
                    <button>Login</button>
                </p>
                <p class="link_register">
                    Register
                </p>
            </form>
    <div class="content-user"></div>
    </center>
</body>
</html>