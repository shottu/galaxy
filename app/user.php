<?php

require '../config/db.php';

class User extends Db
{
	public function dataUser($login)
	{
		$data = $this->pdo->prepare("SELECT * FROM `users` WHERE `login` = :login");
		$data->execute([
				'login' => $login,
		]);
		$user = $data->fetch();

		return $user;
	}

	public function register()
	{
		if (isset($_POST['reg_login']) && isset($_POST['reg_pass'])) {
			if(!empty($_POST['reg_pass'])) {
				if (strlen($_POST['reg_pass']) < 8) {
					echo json_encode([
						'register' => 'error_pass',
					]);
				} else {
					$register = $this->pdo->prepare("INSERT INTO `users` (`login`, `password`) VALUES (:login, :pass)");
					$register->execute([
					'login' => trim($_POST['reg_login']),
					'pass' => password_hash($_POST['reg_pass'], PASSWORD_DEFAULT),
					]);
					echo json_encode([
						'register' => true,
						'user' => $this->dataUser($_POST['reg_login'])['login'],
					]);
				}
			}
		}
	}

	public function login()
	{
		if (isset($_POST['login']) && isset($_POST['pass'])) {
			$login = $this->pdo->prepare("SELECT * FROM `users` WHERE `login` = :login");
			$login->execute([
				'login' => $_POST['login'],
			]);
			$user = $login->fetch();
			if(!empty($_POST['pass'])) {
				if (password_verify($_POST['pass'], $user['password'])) {
					echo json_encode([
						'login' => true,
						'user' => $this->dataUser($user['login'])['login'],
					]);
				} else {
					echo json_encode([
						'login' => 'error_login',
					]);
				}
			}
		}
	}
}

$user = new User();
$user->register();
$user->login();